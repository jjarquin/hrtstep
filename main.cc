#include <signal.h>
#include <boost/shared_ptr.hpp>
#include <alcommon/albroker.h>
#include <alcommon/almodule.h>
#include <alcommon/albrokermanager.h>
#include <alcommon/altoolsmain.h>
#include "tstep_module.h"

#ifdef _WIN32
# define ALCALL __declspec(dllexport)
#else
# define ALCALL
#endif

extern "C"
{
  ALCALL int _createModule(boost::shared_ptr<AL::ALBroker> pBroker)
  {
    // init broker with the main broker instance
    // from the parent executable
    AL::ALBrokerManager::setInstance(pBroker->fBrokerManager.lock());
    AL::ALBrokerManager::getInstance()->addBroker(pBroker);

    // create module instances
    AL::ALModule::createModule<HRTStepModule>(pBroker,"HRTStepModule");
    return 0;
  }

  ALCALL int _closeModule()
  {
    return 0;
  }

} // extern "C"


#ifdef REMOTE_MODULE
  int main(int argc, char *argv[])
  {

      if(argc < 2)
      {
        std::cerr << "Usage: HRTStepModule NAO_IP" << std::endl;
        return 2;
      }

    // pointer to createModule
    TMainType sig;
    sig = &_createModule;
    // call main
    return ALTools::mainFunction("HRTStepModule", argc, argv, sig);
  }
#endif
