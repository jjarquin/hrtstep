#include "tstep_module.h"
#include "nao_interface.h"

#ifdef  USE_LOCAL_XBOT //
    std::string naoFile = "/home/airoa/Thesis_work/naoMove/svnOpenHRC/OpenHRC/src/data/Nao_blue_noninertial.xbot";
#else
    std::string naoFile = "/home/nao/projects/hrData/Nao_blue_noninertial.xbot";
#endif



HRTStepModule::HRTStepModule(boost::shared_ptr<AL::ALBroker> pBroker, const std::string name): AL::ALModule(pBroker, name ){
    setModuleDescription( "Module to make a TStep." );

    functionName("stopProcess", getName() , "stop");
    BIND_METHOD(HRTStepModule::stopProcess);

    functionName("startProcess", getName() , "start");
    BIND_METHOD(HRTStepModule::startProcess);

    functionName("updateModel", getName() , "start");
    BIND_METHOD(HRTStepModule::updateModel);

    functionName("updateRobot", getName() , "start");
    BIND_METHOD(HRTStepModule::updateRobot);

    functionName("resetInternalModel", getName() , "start");
    BIND_METHOD(HRTStepModule::resetInternalModel);

    try{   // Get the ALMotion Proxy
        motionProxy = boost::make_shared<AL::ALMotionProxy>(getParentBroker());
        std::cout << "MotionProxy created" << std::endl;
    }
    catch (AL::ALError& e){
        throw ALERROR(getName(), "startLoop()", "Impossible to create ALMotion Proxy : " + e.toString());
    }


}
HRTStepModule::~HRTStepModule(){

    delete leftFootTask;
    delete rightFootTask;
    delete leftHandTask;
    delete rightHandTask;

    delete leftHandFullTask;
    delete rightHandFullTask;

    delete torsoTask;
    delete torsoZTask;
    delete torsoFullTask;
    delete headTask;
    delete comTask;

    delete solver;
}

void HRTStepModule::init(){
    //! Load multibody
    hr::core::World world = hr::core::World();
    std::string sFile = naoFile;
    int robotId = world.getRobotsVector()->size();
    world.loadMultiBody(naoFile,robotId, hr::core::kNAO);
    robot = world.getRobot(robotId);

    initKinematics();
}

void HRTStepModule::initKinematics(){

    q_hr_r.resize(30); //NAO_DOF + Virtual Bodies
    q_hr_r.setZero();

    q_hr_d.resize(30); //NAO_DOF + Virtual Bodies
    q_hr_d.setZero();

    q_hr_r = getNaoConfig(*motionProxy);
    q_hr_d = q_hr_r;

    //! Set robot configuration and create handles
    robot->setConfiguration(q_hr_r);
    robot->computeForwardKinematics();

    hr::Vector3r rightFootHandle(0.0,0.0,-0.04);
    rightFootHandleId = robot->addOperationalHandle(hr::core::nao::kRightFoot,rightFootHandle);

    hr::Vector3r leftFootHandle(0.0,0.0,-0.04);
    leftFootHandleId = robot->addOperationalHandle(hr::core::nao::kLeftFoot,leftFootHandle);

    hr::Vector3r rightHandHandle(0.08,0.0,0.0);
    rightHandHandleId = robot->addOperationalHandle(hr::core::nao::kRightHand,rightHandHandle);

    hr::Vector3r leftHandHandle(0.08,0.0,0.0);
    leftHandHandleId = robot->addOperationalHandle(hr::core::nao::kLeftHand,leftHandHandle);

    hr::Vector3r torsoHandle(0.0,0.0,0.0);
    torsoHandleId = robot->addOperationalHandle(hr::core::nao::kTorso,torsoHandle);

    hr::Vector3r headHandle(0.0,0.0,0.0);
    headHandleId = robot->addOperationalHandle(hr::core::nao::kHead,headHandle);

    //! Create tasks for end effectors

    leftFootTask = new hr::core::KinematicTask(leftFootHandleId, hr::core::nao::kLeftFoot, 0,robot, hr::core::kPositionOrientation);
    rightFootTask = new hr::core::KinematicTask(rightFootHandleId, hr::core::nao::kRightFoot, 0,robot, hr::core::kPositionOrientation);

    leftHandFullTask = new hr::core::KinematicTask(leftHandHandleId, hr::core::nao::kLeftHand, 0,robot, hr::core::kPositionOrientation);
    rightHandFullTask = new hr::core::KinematicTask(rightHandHandleId, hr::core::nao::kRightHand, 0,robot, hr::core::kPositionOrientation);

    leftHandTask = new hr::core::KinematicTask(leftHandHandleId, hr::core::nao::kLeftHand, 0,robot, hr::core::kPosition);
    rightHandTask = new hr::core::KinematicTask(rightHandHandleId, hr::core::nao::kRightHand, 0,robot, hr::core::kPosition);

    headTask = new hr::core::KinematicTask(headHandleId, hr::core::nao::kHead, 0,robot, hr::core::kWYZ);

    torsoTask = new hr::core::KinematicTask(torsoHandleId, hr::core::nao::kTorso, 0,robot, hr::core::kWXY);
    torsoFullTask = new hr::core::KinematicTask(torsoHandleId, hr::core::nao::kTorso, 0, robot,hr::core::kPositionOrientation);
    torsoZTask = new hr::core::KinematicTask(torsoHandleId,hr::core::nao::kTorso,0,robot,hr::core::kZ);

    comTask = new hr::core::CoMTask(0,robot, hr::core::kXY);

    //! Load end effectors pose

    rightFootPosition = robot->getOperationalHandlePosition(hr::core::nao::kRightFoot,rightFootHandleId);
    rightFootOrientation = robot->getBodyOrientation(hr::core::nao::kRightFoot).eulerAngles(0,1,2);

    leftFootPosition = robot->getOperationalHandlePosition(hr::core::nao::kLeftFoot,leftFootHandleId);
    leftFootOrientation = robot->getBodyOrientation(hr::core::nao::kLeftFoot).eulerAngles(0,1,2);

    rightHandPosition = robot->getOperationalHandlePosition(hr::core::nao::kRightHand,rightHandHandleId);
    rightHandOrientation = robot->getBodyOrientation(hr::core::nao::kRightHand).eulerAngles(0,1,2);

    leftHandPosition = robot->getOperationalHandlePosition(hr::core::nao::kLeftHand,leftHandHandleId);
    leftHandOrientation = robot->getBodyOrientation(hr::core::nao::kLeftHand).eulerAngles(0,1,2);

    torsoPosition = robot->getOperationalHandlePosition(hr::core::nao::kTorso,torsoHandleId);
    torsoOrientation = robot->getBodyOrientation(hr::core::nao::kTorso).eulerAngles(0,1,2);

    headPosition = robot->getOperationalHandlePosition(hr::core::nao::kHead,headHandleId);
    headOrientation = robot->getBodyOrientation(hr::core::nao::kHead).eulerAngles(0,1,2);

    comPosition = robot->getCoM();

    rightFootPose << rightFootPosition , rightFootOrientation;
    leftFootPose << leftFootPosition, leftFootOrientation;

    rightHandPose << rightHandPosition , rightHandOrientation;
    leftHandPose << leftHandPosition , leftHandOrientation;

    torsoPose << torsoPosition, torsoOrientation;
    headPose << headPosition, headOrientation;
    comPose << comPosition , 0,0,0;

    //! Set task goal/trajectory

    rightFootTask->setGoal(rightFootPose);
    leftFootTask->setGoal(leftFootPose);

    rightHandTask->setGoal(rightHandPose);
    leftHandTask->setGoal(leftHandPose);

    rightHandFullTask->setGoal(rightHandPose);
    leftHandFullTask->setGoal(leftHandPose);

    torsoTask->setGoal(torsoPose);
    torsoZTask->setGoal(torsoPose);
    torsoFullTask->setGain(torsoPose);
    headTask->setGoal(headPose);
    comTask->setGoal(comPose);

    comTask->setGain(1.0);

    //! Robot Parameters
    t = 0.0;
    integrationStepSize = 0.01;

    lb_ = (robot->getJointLowLimits()-q_hr_r)/integrationStepSize;
    ub_ = (robot->getJointUpLimits()-q_hr_r)/integrationStepSize;

    lb.resize(29);
    ub.resize(29);

    lb.segment(0,24) = lb_.segment(0,24);
    lb.segment(24,5) = lb_.segment(25,5);

    ub.segment(0,24) = ub_.segment(0,24);
    ub.segment(24,5) = ub_.segment(25,5);

    ld.resize(29);
    ud.resize(29);

    hr::VectorXr ud_ = robot->getJointUpSpeedLimits();
    hr::VectorXr ld_ = robot->getJointLowSpeedLimits();

    int n = 29;
    C = hr::MatrixXr::Identity(n,n);

    ld.segment(0,24) = ld_.segment(0,24);
    ld.segment(24,5) = ld_.segment(25,5);

    ud.segment(0,24) = ud_.segment(0,24);
    ud.segment(24,5) = ud_.segment(25,5);

    //! Init robot motion
    footStatus = hr::core::kBothFeetOnGround;

    //!Init constraints
    supportBaseConstraint = new hr::core::nao::SupportBaseConstraint();
    comJacobian = robot->getCoMJacobian();
    comJacobianXY = comJacobian.topRows<2>();
    comPositionXY = comPosition.head<2>();
    leftFootXYTheta << leftFootPosition(0), leftFootPosition(1), leftFootOrientation(2);
    rightFootXYTheta << rightFootPosition(0), rightFootPosition(1), rightFootOrientation(2);
    supportBaseConstraint->setConstraint(comJacobianXY,comPositionXY,leftFootXYTheta, rightFootXYTheta,footStatus,integrationStepSize);

    //! Print resume
    std::cout << "Left Foot goal: " << std::endl << leftFootPose << std::endl;
    std::cout << "Right Foot goal: " << std::endl << rightFootPose << std::endl;
    std::cout << "Torso goal: " << std::endl << torsoPose << std::endl;

    std::cout << "Left Hand goal: " << std::endl << leftHandPose << std::endl;
    std::cout << "Right Hand goal: " << std::endl << rightHandPose << std::endl;
    std::cout << "Head goal: " << std::endl << headPose << std::endl;

    std::cout << "CoM goal: " << std::endl << comPosition << std::endl;

    std::cout << "Robot configuration" << std::endl << ((180.0/(3.1415))*q_hr_r.transpose()) << std::endl;


}

void HRTStepModule::updateModel(){

    //! update q_hr_d values
    //if(updateValues){
      //  //update q_hr_d with q_hr_r
     //   updateValues = false;
    //}

    // Read sensor values
    q_hr_r = getNaoConfig(*motionProxy);
    q_hr_r.segment(0,6) = q_hr_d.segment(0,6);

    robot->setConfiguration(q_hr_r);
    robot->computeForwardKinematics();


    rightFootPosition = robot->getOperationalHandlePosition(hr::core::nao::kRightFoot,rightFootHandleId);
    rightFootOrientation = robot->getBodyOrientation(hr::core::nao::kRightFoot).eulerAngles(0,1,2);

    leftFootPosition = robot->getOperationalHandlePosition(hr::core::nao::kLeftFoot,leftFootHandleId);
    leftFootOrientation = robot->getBodyOrientation(hr::core::nao::kLeftFoot).eulerAngles(0,1,2);

    rightHandPosition = robot->getOperationalHandlePosition(hr::core::nao::kRightHand,rightHandHandleId);
    rightHandOrientation = robot->getBodyOrientation(hr::core::nao::kRightHand).eulerAngles(0,1,2);

    leftHandPosition = robot->getOperationalHandlePosition(hr::core::nao::kLeftHand,leftHandHandleId);
    leftHandOrientation = robot->getBodyOrientation(hr::core::nao::kLeftHand).eulerAngles(0,1,2);

    torsoPosition = robot->getOperationalHandlePosition(hr::core::nao::kTorso,torsoHandleId);
    torsoOrientation = robot->getBodyOrientation(hr::core::nao::kTorso).eulerAngles(0,1,2);

    headPosition = robot->getOperationalHandlePosition(hr::core::nao::kHead,headHandleId);
    headOrientation = robot->getBodyOrientation(hr::core::nao::kHead).eulerAngles(0,1,2);

    comPosition = robot->getCoM();

    rightFootPose << rightFootPosition , rightFootOrientation;
    leftFootPose << leftFootPosition, leftFootOrientation;

    rightHandPose << rightHandPosition , rightHandOrientation;
    leftHandPose << leftHandPosition , leftHandOrientation;

    torsoPose << torsoPosition, torsoOrientation;
    headPose << headPosition, headOrientation;

    comPose << comPosition , 0,0,0;

    //! Calculate the new joint speed limit
    lb_ = (robot->getJointLowLimits()-q_hr_r)/integrationStepSize;
    ub_ = (robot->getJointUpLimits()-q_hr_r)/integrationStepSize;

    lb.segment(0,24) = lb_.segment(0,24);
    lb.segment(24,5) = lb_.segment(25,5);

    ub.segment(0,24) = ub_.segment(0,24);
    ub.segment(24,5) = ub_.segment(25,5);

    //! Update constraint variables
    comJacobian = robot->getCoMJacobian();
    comJacobianXY = comJacobian.topRows<2>();
    comPositionXY = comPosition.head<2>();
    leftFootXYTheta << leftFootPosition(0), leftFootPosition(1), leftFootOrientation(2);
    rightFootXYTheta << rightFootPosition(0), rightFootPosition(1), rightFootOrientation(2);
    supportBaseConstraint->setConstraint(comJacobianXY,comPositionXY,leftFootXYTheta, rightFootXYTheta,footStatus,integrationStepSize);



}

void HRTStepModule::updateRobot(){

    //! Update tasks
    leftHandTask->update(t);
    rightHandTask->update(t);

    leftHandFullTask->update(t);
    rightHandFullTask->update(t);

    leftFootTask->update(t);
    rightFootTask->update(t);

    torsoTask->update(t);
    torsoZTask->update(t);
    torsoFullTask->update(t);

    headTask->update(t);
    comTask->update(t);

    //// Need a task handler here as the order and priority may change

    //! Append tasks to the taskList

    std::vector<hr::utils::TaskAbstract*> taskList;
    hr::utils::SimpleTask swingFootTask;
    hr::utils::SimpleTask feetTask;
    hr::utils::SimpleTask handsTask;
    hr::utils::SimpleTask handsTorsoHeadTask;

    hr::VectorXr zeros(6);
    zeros.setZero();

    //Fix hands wrt to torso
    hr::MatrixXr righHandJacobian = rightHandFullTask->getMatrixA();
    hr::MatrixXr leftHandJacobian = leftHandFullTask->getMatrixA();
    righHandJacobian.leftCols(6).setZero();
    leftHandJacobian.leftCols(6).setZero();

    if(footStatus == hr::core::kBothFeetOnGround){

        handsTask.addRows(righHandJacobian,zeros);
        handsTask.addRows(leftHandJacobian,zeros);

        feetTask.addRows(leftFootTask->getMatrixA(),leftFootTask->getVectorb());
        feetTask.addRows(rightFootTask->getMatrixA(),rightFootTask->getVectorb());

        taskList.push_back(&feetTask);
        taskList.push_back(&handsTask);
        taskList.push_back(headTask);
        taskList.push_back(torsoZTask);
        taskList.push_back(comTask);

    }
    else if(footStatus == hr::core::kRightFootOnGround){
        handsTask.addRows(righHandJacobian,zeros);
        handsTask.addRows(leftHandJacobian,zeros);

        swingFootTask.addRows(leftFootTask->getMatrixA(),leftFootTask->getVectorb());


        feetTask.addRows(leftFootTask->getMatrixA(),leftFootTask->getVectorb());
        feetTask.addRows(rightFootTask->getMatrixA(),rightFootTask->getVectorb());

        taskList.push_back(&feetTask);

        handsTorsoHeadTask.addRows(handsTask.getMatrixA(),handsTask.getVectorb());
        handsTorsoHeadTask.addRows(headTask->getMatrixA(),headTask->getVectorb());
        handsTorsoHeadTask.addRows(torsoZTask->getMatrixA(),torsoZTask->getVectorb());

        taskList.push_back(&handsTorsoHeadTask);

        taskList.push_back(comTask);




    }
    else if(footStatus == hr::core::kLeftFootOnGround){
        handsTask.addRows(righHandJacobian,zeros);
        handsTask.addRows(leftHandJacobian,zeros);

        swingFootTask.addRows(rightFootTask->getMatrixA(),rightFootTask->getVectorb());


        feetTask.addRows(leftFootTask->getMatrixA(),leftFootTask->getVectorb());
        feetTask.addRows(rightFootTask->getMatrixA(),rightFootTask->getVectorb());

        taskList.push_back(&feetTask);

        handsTorsoHeadTask.addRows(handsTask.getMatrixA(),handsTask.getVectorb());
        handsTorsoHeadTask.addRows(headTask->getMatrixA(),headTask->getVectorb());
        handsTorsoHeadTask.addRows(torsoZTask->getMatrixA(),torsoZTask->getVectorb());

        taskList.push_back(&handsTorsoHeadTask);
        taskList.push_back(comTask);

    }



    //! Solve inverse kinematics
    hr::VectorXr dq = solver->Solve(taskList,C,ld,ud,lb,ub);

    //! Integrate
    integrateNaoSpeed(q_hr_d,dq,integrationStepSize);
    t = t + integrationStepSize;

    setNaoConfig(*motionProxy,q_hr_d,integrationStepSize);

}

void HRTStepModule::resetInternalModel(){
    initKinematics();

}

void HRTStepModule::startProcess(){
    motionProxy->walkInit();
    resetInternalModel();

    hr::real_t z_obstacle = 0.05;

    // Begin      :  Goto StandInit;
    hr::real_t t0 = 0.0;  // t0 to t1 :  Torso at 0.0
    hr::real_t t1 = 1.0;  // t1 to t2 :  Com in RightFoot
    hr::real_t t2 = 3.0;  // t2 to t3 :  Step LeftFoot at 90DEGREES
    hr::real_t t3 = 10.0;  // t3 to t4 :  Com in LeftFoot
    hr::real_t t3_1 = 13.0; // t3 to t4 : Lower torso
    hr::real_t t4 = 14.0; // t4 to t5 :  Step RightFoot at FRONT
    hr::real_t t5 = 19.0; // t5 to t6 :  Com in RightFoot
    hr::real_t t6 = 25.0; // t6 to t7 :  Step LeftFoot at FRONT
    hr::real_t t7 = 31.0; // t7 to t8 :  Com in Center and Raise Torso
    hr::real_t t8 = 33.0; // --
     // End        :  Goto StandInit;

    hr::real_t eps = 0.000001;
    t = 0.0;
    hr::real_t T = t6;
    int N = T/integrationStepSize;


    for(int i = 0; i != N; i++){


        updateModel();



        //Update tasks according to the time

        if(i == static_cast<int>(t0/integrationStepSize))
        {
            //::step_00

            std::cout << "TorsoPose" << torsoPose.transpose() << std::endl;
            std::cout << "RightFootPose" << rightFootPose.transpose() << std::endl;
            std::cout << "LeftFootPose" << leftFootPose.transpose() << std::endl;
            std::cout << "comPose" << comPose.transpose() << std::endl;


            //Torso orientation at (0, 0 , 0) degrees
            std::cout << "Time " << t0 << std::endl;
            std::cout << "Init stepOver task" << std::endl;

            eps = 0.000001;
        }
        if(i == static_cast<int>(t1/integrationStepSize))
        {
            //::step_01


            std::cout << "TorsoPose" << torsoPose.transpose() << std::endl;
            std::cout << "RightFootPose" << rightFootPose.transpose() << std::endl;
            std::cout << "LeftFootPose" << leftFootPose.transpose() << std::endl;
            std::cout << "comPose" << comPose.transpose() << std::endl;

            //CoM in the right foot
            std::cout << "Time " << t1 << std::endl;
            std::cout << "CoM in rightFoot" << std::endl;

            hr::SpatialVector cGoal;
            cGoal <<  rightFootPosition(0)-0.01, rightFootPosition(1)+0.01, comPosition(2), 0.0,0.0,0.0;

            boost::shared_ptr<hr::core::PointTrajectory> comTraj = boost::make_shared<hr::core::PointTrajectory>(t1,t2,comPose,cGoal,hr::core::kSepticPolynomial);
            comTask->setTrajectory(comTraj);

            hr::SpatialVector rightFootGoal;
            hr::SpatialVector leftFootGoal;
            rightFootGoal << rightFootPosition , 0,0,0.0;
            leftFootGoal << leftFootPosition, 0,0,0;
            rightFootTask->setGoal(rightFootGoal);
            leftFootTask->setGoal(leftFootGoal);

            eps = 0.000001;
        }
        if(i == static_cast<int>(t2/integrationStepSize))
        {
            //::step_02


            std::cout << "TorsoPose" << torsoPose.transpose() << std::endl;
            std::cout << "RightFootPose" << rightFootPose.transpose() << std::endl;
            std::cout << "LeftFootPose" << leftFootPose.transpose() << std::endl;
            std::cout << "comPose" << comPose.transpose() << std::endl;

            //Left foot at 90 degrees
            std::cout << "Time " << t2 << std::endl;
            std::cout << "leftFoot at 90degrees" << std::endl;

            hr::SpatialVector lFootGoal;
            lFootGoal << leftFootPosition(0)+0.04, leftFootPosition(1)+0.005, rightFootPosition(2), 0,0,1.5707;
            boost::shared_ptr<hr::core::FootTrajectory> leftFootTraj = boost::make_shared<hr::core::FootTrajectory>(t2,t3,leftFootPose,lFootGoal,hr::core::kFootCycloidal,hr::core::kFixed,0.03);
            leftFootTask->setTrajectory(leftFootTraj);

            hr::SpatialVector torsoGoal;
            torsoGoal << torsoPosition(0),torsoPosition(1),torsoPosition(2), torsoOrientation;
            torsoZTask->setGoal(torsoGoal);

            hr::SpatialVector rightFootGoal;
            rightFootGoal << rightFootPosition , 0,0,0.0;
            rightFootTask->setGoal(rightFootGoal);

            eps = 0.00001;
        }

        if(i == static_cast<int>(t3/integrationStepSize))
        {

            //::step_03

            std::cout << "TorsoPose" << torsoPose.transpose() << std::endl;
            std::cout << "RightFootPose" << rightFootPose.transpose() << std::endl;
            std::cout << "LeftFootPose" << leftFootPose.transpose() << std::endl;
            std::cout << "comPose" << comPose.transpose() << std::endl;

            //com in the left foot
            std::cout << "Time " << t3 << std::endl;
            std::cout << "CoM in leftFoot" << std::endl;

            hr::SpatialVector cGoal;
            cGoal <<  leftFootPosition(0)+0.01, leftFootPosition(1)-0.015, comPosition(2), 0.0,0.0,0.0;
            boost::shared_ptr<hr::core::PointTrajectory> comTraj = boost::make_shared<hr::core::PointTrajectory>(t3,t4,comPose,cGoal,hr::core::kSepticPolynomial);
            comTask->setTrajectory(comTraj);


            eps = 0.00001;
        }

        if(i == static_cast<int>(t3_1/integrationStepSize)){

            //::step_04

            std::cout << "TorsoPose" << torsoPose.transpose() << std::endl;
            std::cout << "RightFootPose" << rightFootPose.transpose() << std::endl;
            std::cout << "LeftFootPose" << leftFootPose.transpose() << std::endl;
            std::cout << "comPose" << comPose.transpose() << std::endl;

            //torso adjustment
            std::cout << "Time " << t3 << std::endl;
            std::cout << "CoM lower" << std::endl;

            hr::SpatialVector rightFootGoal;
            hr::SpatialVector leftFootGoal;
            hr::SpatialVector torsoGoal;

            leftFootGoal << leftFootPosition,0,0,1.5707;
            leftFootTask->setGoal(leftFootGoal);

            rightFootGoal << rightFootPosition,0,0,0;
            rightFootTask->setGoal(rightFootGoal);


            torsoGoal << torsoPosition(0),torsoPosition(1),torsoPosition(2)-0.02, torsoOrientation;
            torsoZTask->setGoal(torsoGoal);
            eps = 0.00001;

        }

        if(i == static_cast<int>(t4/integrationStepSize))
        {

            //::step_05

            std::cout << "TorsoPose" << torsoPose.transpose() << std::endl;
            std::cout << "RightFootPose" << rightFootPose.transpose() << std::endl;
            std::cout << "LeftFootPose" << leftFootPose.transpose() << std::endl;
            std::cout << "comPose" << comPose.transpose() << std::endl;

            //Right foot to the front
            std::cout << "Time " << t4 << std::endl;
            std::cout << "Right foot to the front" << std::endl;

            std::vector<hr::real_t> times;
            std::vector<hr::SpatialVector> points;
            hr::SpatialVector rightFootPose_01;
            hr::SpatialVector rightFootPose_02;
            hr::SpatialVector rFootGoal;
            rightFootPose_01 << rightFootPosition(0)-0.02, rightFootPosition(1), rightFootPosition(2) + z_obstacle-0.01 , 0,0.3,0;
            rightFootPose_02 << rightFootPosition(0)+0.15, rightFootPosition(1)+0.02, rightFootPosition(2) + z_obstacle , 0,0,-0.2;
            rFootGoal <<        rightFootPosition(0)+0.20, rightFootPosition(1)+0.06, rightFootPosition(2)  , 0,0, 0.0;

            times.push_back(t4);
            times.push_back(t4 + (t5-t4)/3.0);
            times.push_back(t4 + 2.0*(t5-t4)/3.0);
            times.push_back(t5);

            points.push_back(rightFootPose);
            points.push_back(rightFootPose_01);
            points.push_back(rightFootPose_02);
            points.push_back(rFootGoal);

            std::vector<hr::SpatialVector> initialConditions;
            std::vector<hr::SpatialVector> finalConditions;

            boost::shared_ptr<hr::core::MultiplePointTrajectory> rightFootTraj = boost::make_shared<hr::core::MultiplePointTrajectory>(times,points,initialConditions,finalConditions);
            rightFootTask->setTrajectory(rightFootTraj);

            hr::SpatialVector torsoGoal;
            torsoGoal << torsoPosition(0),torsoPosition(1),torsoPosition(2), torsoOrientation;
            torsoZTask->setGoal(torsoGoal);
            torsoTask->setGoal(torsoGoal);
            comTask->setGoal(comPose);
            comTask->setGain(0.0);
            eps = 0.00001;
        }

        if(i == static_cast<int>(t5/integrationStepSize))
        {
            std::cout << "TorsoPose" << torsoPose.transpose() << std::endl;
            std::cout << "RightFootPose" << rightFootPose.transpose() << std::endl;
            std::cout << "LeftFootPose" << leftFootPose.transpose() << std::endl;
            std::cout << "comPose" << comPose.transpose() << std::endl;


            std::cout << "Time " << t5 << std::endl;
            std::cout << "CoM in Right Foot" << std::endl;
            //com in the right foot
            std::vector<hr::real_t> times;
            std::vector<hr::SpatialVector> points;

            hr::SpatialVector cGoal;
            hr::SpatialVector cInner_01;
            hr::SpatialVector cInner_02;
            cInner_01 << comPosition(0), comPosition(1) + 0.02 , comPosition(2),0,0,0;
            cInner_02 << comPosition(0)+ 0.01, comPosition(1) + 0.03 , comPosition(2),0,0,0;
            cGoal <<  rightFootPosition(0)-0.02, rightFootPosition(1), comPosition(2), 0.0,0.0,0.0;

            times.push_back(t5);
            times.push_back(t5+0.25*(t6-t5));
            times.push_back(t5+0.50*(t6-t5));
            times.push_back(t6);

            points.push_back(comPose);
            points.push_back(cInner_01);
            points.push_back(cInner_02);
            points.push_back(cGoal);

            std::vector<hr::SpatialVector> initialConditions;
            std::vector<hr::SpatialVector> finalConditions;
            boost::shared_ptr<hr::core::MultiplePointTrajectory> comTraj = boost::make_shared<hr::core::MultiplePointTrajectory>(times,points,initialConditions,finalConditions);

            comTask->setTrajectory(comTraj);
        }
        if(i == static_cast<int>(t6/integrationStepSize))
        {

            std::cout << "Time " << t6 << std::endl;
            std::cout << "Step Left Foot" << std::endl;

            //! Setting a bezier for the foot
            std::vector<hr::real_t> times;
            std::vector<hr::SpatialVector> points;
            hr::SpatialVector leftFootPose_01;
            hr::SpatialVector leftFootPose_02;
            hr::SpatialVector lFootGoal;

            leftFootPose_01 << leftFootPosition(0)+0.02, leftFootPosition(1)+0.01, leftFootPosition(2) + 0.05, leftFootOrientation;
            leftFootPose_02 << rightFootPosition(0) , rightFootPosition(1)+0.09, leftFootPosition(2) + 0.07 , 0,0,0.3;
            lFootGoal << rightFootPosition(0), rightFootPosition(1)+0.10, leftFootPosition(2), 0,  0,0.0;

            times.push_back(t6);
            times.push_back(t6 + (t7-t6)/3.0);
            times.push_back(t6 + 2*(t7-t6)/3.0);
            times.push_back(t7);

            points.push_back(leftFootPose);
            points.push_back(leftFootPose_01);
            points.push_back(leftFootPose_02);
            points.push_back(lFootGoal);

            std::vector<hr::SpatialVector> initialConditions;
            std::vector<hr::SpatialVector> finalConditions;

            boost::shared_ptr<hr::core::MultiplePointTrajectory> leftFootTraj = boost::make_shared<hr::core::MultiplePointTrajectory>(times,points,initialConditions,finalConditions);
            leftFootTask->setTrajectory(leftFootTraj);

            hr::SpatialVector torsoGoal;
            torsoGoal << torsoPosition(0),torsoPosition(1),torsoPosition(2), 0,0,0;
            torsoZTask->setGoal(torsoGoal);
            rightFootTask->setTaskSpace(hr::core::kXZ);

        }

        if(i == static_cast<int>(t7/integrationStepSize))
        {
            std::cout << "Time " << t7 << std::endl;
            std::cout << "Center" << std::endl;

            hr::SpatialVector cGoal;
            cGoal <<  0.5*(rightFootPosition(0)+leftFootPosition(0)), 0.5*(rightFootPosition(1)+leftFootPosition(1)), comPosition(2), 0.0,0.0,0.0;

            boost::shared_ptr<hr::core::PointTrajectory> comTraj = boost::make_shared<hr::core::PointTrajectory>(t7,t8,comPose,cGoal,hr::core::kSepticPolynomial);
            comTask->setTrajectory(comTraj);
            hr::SpatialVector torsoGoal;
            torsoGoal << torsoPosition(0),torsoPosition(1),0.0, 0,0,0;
            torsoZTask->setGoal(torsoGoal);

        }



        //! Update tasks
        leftHandTask->update(t);
        rightHandTask->update(t);

        leftHandFullTask->update(t);
        rightHandFullTask->update(t);

        leftFootTask->update(t);
        rightFootTask->update(t);

        torsoTask->update(t);
        torsoZTask->update(t);
        torsoFullTask->update(t);

        headTask->update(t);
        comTask->update(t);


        //Create taskList and compound tasks
        std::vector<hr::utils::TaskAbstract*> taskList;
        hr::utils::SimpleTask feetTask;
        hr::utils::SimpleTask handsTask;
        hr::utils::SimpleTask comConstrainedTask;

        //Fix hands wrt to torso
        hr::VectorXr zeros(6);
        zeros.setZero();
        hr::MatrixXr righHandJacobian = rightHandFullTask->getMatrixA();
        hr::MatrixXr leftHandJacobian = leftHandFullTask->getMatrixA();
        righHandJacobian.leftCols(6).setZero();
        leftHandJacobian.leftCols(6).setZero();
        handsTask.addRows(righHandJacobian,zeros);
        handsTask.addRows(leftHandJacobian,zeros);

        //Left and Right foot with the same priority
        feetTask.addRows(leftFootTask->getMatrixA(),leftFootTask->getVectorb());
        feetTask.addRows(rightFootTask->getMatrixA(),rightFootTask->getVectorb());



        if(i >= static_cast<int>(t7/integrationStepSize))
        {

            //com in center

            taskList.push_back(&feetTask);
            taskList.push_back(&handsTask);
            taskList.push_back(headTask);
            taskList.push_back(torsoZTask);
            taskList.push_back(comTask);
            taskList.push_back(torsoTask);

        }
        else if(i >= static_cast<int>(t6/integrationStepSize))
        {
            //step left foot and set near to right
            std::cout << "in t6 - t7 " << std::endl;

            supportBaseConstraint->setConstraint(comJacobianXY,comPositionXY,leftFootXYTheta,rightFootXYTheta,hr::core::kRightFootOnGround,integrationStepSize);
            comConstrainedTask.addRows(feetTask.getMatrixA(),feetTask.getVectorb());
            comConstrainedTask.addConstraintRows(supportBaseConstraint->getMatrixC(),supportBaseConstraint->getVectord());

            taskList.push_back(&handsTask);
            taskList.push_back(&comConstrainedTask);
            taskList.push_back(headTask);
            taskList.push_back(torsoZTask);

        }
        else if(i >= static_cast<int>(t5/integrationStepSize))
        {

            //com towards right foot
            std::cout << "in t5 - t6 " << std::endl;

            supportBaseConstraint->setConstraint(comJacobianXY,comPositionXY,leftFootXYTheta,rightFootXYTheta,hr::core::kBothFeetOnGround,integrationStepSize);
            comConstrainedTask.addRows(comTask->getMatrixA(),comTask->getVectorb());
            comConstrainedTask.addConstraintRows(supportBaseConstraint->getMatrixC(),supportBaseConstraint->getVectord());


            taskList.push_back(&feetTask);
            taskList.push_back(&handsTask);
            taskList.push_back(headTask);
            taskList.push_back(&comConstrainedTask);
        }
        else if(i >= static_cast<int>(t4/integrationStepSize))
        {
            //::step_05
            std::cout << "in t4 - t5 " << std::endl;

            //step right to front

            supportBaseConstraint->setConstraint(comJacobianXY,comPositionXY,leftFootXYTheta,rightFootXYTheta,hr::core::kLeftFootOnGround,integrationStepSize);
            comConstrainedTask.addRows(rightFootTask->getMatrixA(),rightFootTask->getVectorb());
            comConstrainedTask.addConstraintRows(supportBaseConstraint->getMatrixC(),supportBaseConstraint->getVectord());

            taskList.push_back(leftFootTask);
            taskList.push_back(&handsTask);
            taskList.push_back(headTask);
            taskList.push_back(comTask);
            taskList.push_back(torsoZTask);
            taskList.push_back(rightFootTask);

        }
        else if(i >= static_cast<int>(t3_1/integrationStepSize))
        {
            //::step_04

            std::cout << "in t3_1 - t4 " << std::endl;
            //com adjustment

            supportBaseConstraint->setConstraint(comJacobianXY,comPositionXY,leftFootXYTheta,rightFootXYTheta,hr::core::kBothFeetOnGround,integrationStepSize);
            comConstrainedTask.addRows(feetTask.getMatrixA(),feetTask.getVectorb());
            comConstrainedTask.addConstraintRows(supportBaseConstraint->getMatrixC(),supportBaseConstraint->getVectord());


            taskList.push_back(&feetTask);
            taskList.push_back(torsoZTask);
        }

        else if(i >= static_cast<int>(t3/integrationStepSize))
        {
            //::step_03

            std::cout << "in t3 - t3_1" << std::endl;
            //com towards left foot

            supportBaseConstraint->setConstraint(comJacobianXY,comPositionXY,leftFootXYTheta,rightFootXYTheta,hr::core::kBothFeetOnGround,integrationStepSize);
            comConstrainedTask.addRows(feetTask.getMatrixA(),feetTask.getVectorb());
            comConstrainedTask.addConstraintRows(supportBaseConstraint->getMatrixC(),supportBaseConstraint->getVectord());


            taskList.push_back(&feetTask);
            taskList.push_back(headTask);
            taskList.push_back(comTask);
        }

        else if(i >= static_cast<int>(t2/integrationStepSize))
        {

            //::step_02
            std::cout << "in t2 - t3 " << std::endl;

            supportBaseConstraint->setConstraint(comJacobianXY,comPositionXY,leftFootXYTheta,rightFootXYTheta,hr::core::kRightFootOnGround,integrationStepSize);
            comConstrainedTask.addRows(torsoZTask->getMatrixA(),torsoZTask->getVectorb());
            comConstrainedTask.addConstraintRows(supportBaseConstraint->getMatrixC(),supportBaseConstraint->getVectord());;

            taskList.push_back(&feetTask);
            taskList.push_back(&handsTask);
            taskList.push_back(headTask);
            taskList.push_back(&comConstrainedTask);
            taskList.push_back(torsoTask);

        }

        else if(i >= static_cast<int>(t1/integrationStepSize))
        {
            //::step_01
            std::cout << "int t1 - t2 " << std::endl;

            //com towards right foot
            taskList.push_back(&feetTask);
            taskList.push_back(comTask);
            taskList.push_back(torsoTask);
        }

        else if(i >= static_cast<int>(t0/integrationStepSize))
        {
            //::step_00
            std::cout << "int t0 - t1 " << std::endl;

            //init
            taskList.push_back(&feetTask);
            taskList.push_back(&handsTask);
            taskList.push_back(headTask);
            taskList.push_back(torsoTask);
        }


        //! Solve inverse kinematics
        hr::VectorXr dq = solver->Solve(taskList,C,ld,ud,lb,ub,eps);

        //! Integrate
        integrateNaoSpeed(q_hr_d,dq,integrationStepSize);
        t = t + integrationStepSize;

        setNaoConfig(*motionProxy,q_hr_d,integrationStepSize);

    }


}

void HRTStepModule::stopProcess(){

}


