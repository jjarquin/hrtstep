#ifndef TSTEP_MODULE_H
#define TSTEP_MODULE_H

//cpp includes
#include <string>
#include <boost/shared_ptr.hpp>
#include <boost/timer/timer.hpp>

//eigen
#include "Eigen/Dense"

//al includes
#include <alcommon/almodule.h>
#include <alproxies/almotionproxy.h>


//openhrc includes
#include "openhrc/core.h"
#include "openhrc/kinematics.h"
#include "openhrc/utils.h"


using boost::timer::cpu_timer;

namespace AL{
class ALBroker;
class ALMotion;
}

class HRTStepModule : public AL::ALModule{
public:
    HRTStepModule(boost::shared_ptr<AL::ALBroker> pBroker, const std::string name);
    virtual ~HRTStepModule();

    void init(); //Method called when module is created


    void startProcess();
    void stopProcess();



private:

    void initKinematics();  // load initial configuration and init kinematics
    void updateModel();
    void updateRobot();
    void resetInternalModel();

    boost::shared_ptr<AL::ALMotionProxy> motionProxy;

    boost::shared_ptr<hr::core::MultiBody> robot; // Robot Multibody

    hr::utils::ConstrainedHierarchicalSolver *solver;

    hr::VectorXr q_hr_r; //robot configuration read from sensors.
    hr::VectorXr q_hr_d; //robot configuration used to send position commands to the robot.

    int rightFootHandleId;
    int leftFootHandleId;

    int rightHandHandleId;
    int leftHandHandleId;

    int torsoHandleId;
    int headHandleId;

    hr::core::KinematicTask *leftFootTask;
    hr::core::KinematicTask *rightFootTask;
    hr::core::KinematicTask *leftHandTask;
    hr::core::KinematicTask *rightHandTask;

    hr::core::KinematicTask *leftHandFullTask;
    hr::core::KinematicTask *rightHandFullTask;

    hr::core::KinematicTask *torsoTask;
    hr::core::KinematicTask *torsoZTask;
    hr::core::KinematicTask *torsoFullTask;
    hr::core::KinematicTask *headTask;
    hr::core::CoMTask* comTask;

    hr::Vector3r rightFootPosition;
    hr::Vector3r rightFootOrientation;

    hr::Vector3r leftFootPosition;
    hr::Vector3r leftFootOrientation;

    hr::Vector3r rightHandPosition;
    hr::Vector3r rightHandOrientation;

    hr::Vector3r leftHandPosition;
    hr::Vector3r leftHandOrientation;

    hr::Vector3r torsoPosition;
    hr::Vector3r torsoOrientation;

    hr::Vector3r headPosition;
    hr::Vector3r headOrientation;

    hr::Vector3r comPosition;

    hr::SpatialVector rightFootPose;
    hr::SpatialVector leftFootPose;

    hr::SpatialVector rightHandPose;
    hr::SpatialVector leftHandPose;

    hr::SpatialVector torsoPose;
    hr::SpatialVector headPose;

    hr::SpatialVector comPose;

    hr::core::FootStatus footStatus;

    cpu_timer timer;
    cpu_timer timer_iteration;

    hr::real_t integrationStepSize;

    hr::MatrixXr C;

    //! Lower velocity limits
    hr::VectorXr ld;

    //! Upper velocity limits
    hr::VectorXr ud;

    hr::VectorXr lb_;
    hr::VectorXr ub_;

    //! Lower Bounds for the Prioritized Inverse Kinematic Control
    hr::VectorXr lb;

    //! Upper Bounds for the Prioritized Inverse Kinematic Control
    hr::VectorXr ub;

    //! Time holder variable
    hr::real_t t;

    //! Nao support base Constraint;
    hr::core::nao::SupportBaseConstraint * supportBaseConstraint;

    hr::Matrix3Xr comJacobian;
    hr::Matrix2Xr comJacobianXY;
    hr::Vector2r comPositionXY;
    hr::Vector3r leftFootXYTheta;
    hr::Vector3r rightFootXYTheta;


};

#endif //TSTEP_MODULE_H
