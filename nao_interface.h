#ifndef NAO_INTERFACE_H
#define NAO_INTERFACE_H

#include "openhrc/core.h"
#include "alproxies/almotionproxy.h"

hr::VectorXr getNaoConfig(AL::ALMotionProxy & motionProxy);
bool setNaoConfig(AL::ALMotionProxy & motionProxy, const hr::VectorXr & q, hr::real_t time );
void integrateNaoSpeed(hr::VectorXr & q_hr, const hr::VectorXr & dq_hr, double integrationStepSize);

void hrToAl(const hr::VectorXr & q_hr , hr::VectorXr & q_al){
    const int kNAO_DOF = 26;
    int jointId[kNAO_DOF] = {7,8,9,10,11,12,13,-1,19,20,21,22,23,24,25,26,27,28,29,30,14,15,16,17,18,-1};
    q_al.setZero();
    for (int i = 0 ; i < kNAO_DOF; i++) {
        if(jointId[i] != -1) {
            q_al(i) = q_hr(jointId[i]-1);
        }
    }

}

void alToHr(hr::VectorXr & q_hr , const hr::VectorXr & q_al){
    const int kNAO_DOF = 26;
    int jointId[kNAO_DOF] = {7,8,9,10,11,12,13,-1,19,20,21,22,23,24,-1,26,27,28,29,30,14,15,16,17,18,-1};
    q_hr.setZero();
    for (int i = 0 ; i < kNAO_DOF; i++) {
        if(jointId[i] != -1) {
            q_hr(jointId[i]-1) = q_al(i);
        }
    }
    q_hr(24) = q_hr(18);

}

hr::VectorXr getNaoConfig(AL::ALMotionProxy & motionProxy){
    const int kNAO_DOF = 26;
    const bool useSensors = true;

    AL::ALValue jointNames;
    jointNames = AL::ALValue::array("Body");
    std::vector<float> sensorAngles = motionProxy.getAngles(jointNames, useSensors);
    Eigen::Map<Eigen::VectorXf> q_al(sensorAngles.data(),26);

    hr::VectorXr q_hr(30);
    alToHr(q_hr, q_al.cast<hr::real_t>());
    return q_hr;

}

bool setNaoConfig(AL::ALMotionProxy & motionProxy, const hr::VectorXr & q_hr, hr::real_t time ){
    const int kNAO_DOF = 26;
    const bool isAbsolute = true;

    AL::ALValue alAngles, timeLists;
    alAngles.clear();
    alAngles.arraySetSize(kNAO_DOF);

    timeLists.clear();
    timeLists.arraySetSize(kNAO_DOF);

    hr::VectorXr q_al(kNAO_DOF);

    hrToAl(q_hr,q_al);


    AL::ALValue jointNames;
    jointNames = AL::ALValue::array("Body");
    for (int i = 0; i!=kNAO_DOF; i++) {
        timeLists[i] = AL::ALValue::array(static_cast<float>(time));
        alAngles[i] = q_al(i);
    }
    motionProxy.setAngles(jointNames,alAngles,1.0);
    //motionProxy.angleInterpolation(jointNames, alAngles, timeLists, isAbsolute);

    return true;

}

void integrateNaoSpeed(hr::VectorXr & q_hr, const hr::VectorXr & dq_hr, double integrationStepSize){
    hr::VectorXr qtmp(29);
    qtmp.setZero();

    qtmp.segment(0,24) = q_hr.segment(0,24);
    qtmp.segment(24,5) = q_hr.segment(25,5);
    qtmp += dq_hr*integrationStepSize;

    q_hr.segment(0,24) = qtmp.segment(0,24);
    q_hr(24) = qtmp(18);
    q_hr.segment(25,5) = qtmp.segment(24,5);
}

#endif // NAO_INTERFACE_H
